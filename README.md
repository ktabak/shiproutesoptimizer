# Ship Routes Optimizer

This application, based on the provided ship routes, generates two GeoJSON files: 
* first contains representative points - found with optimization squares algorithm,
* second contains representative route - found by taking route with the median length

Both methods have their drawbacks, an idea for the future would be to use vectors built on the directions of ship movements.

Actual implementation supports only GeoJSON files, however there is a possibility of extending to other file formats in the future.

## How to run?
To run application please type 
```./gradlew bootRun```

As default, you can find generated GeoJSON files in ```"src/main/resources/"```

* Note: If you do not use local gradle, you mau need to run ```./gradlew wrapper```


## How to test?
To run tests please type
```./gradlew test```
* The unit tests were created for 3 classes containing business logic.
* In the future, there should also be tests for "Processor" classes and the "FormatHandler"

## How to configure?
In resources/application.yml file you can specify:
* ```consoleRunner.importRoutesFilePath``` - GeoJSON file with routes to optimize
* ```consoleRunner.exportPointsFilePath``` - GeoJSON file with representative routes points
* ```consoleRunner.exportRouteFilePath``` - GeoJSON file with representative route
* ```representativePointsProcessor.squareAggregatorSideLength``` - side length in degrees of optimization squares
* ```representativePointsProcessor.squareAggregatorMinOccurrences``` - minimum number of routes points occurrences in optimization squares
