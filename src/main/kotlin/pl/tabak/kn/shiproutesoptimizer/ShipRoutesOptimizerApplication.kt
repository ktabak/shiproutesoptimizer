package pl.tabak.kn.shiproutesoptimizer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShipRoutesOptimizerApplication

fun main(args: Array<String>) {
    runApplication<ShipRoutesOptimizerApplication>(*args)
}
