package pl.tabak.kn.shiproutesoptimizer.exceptions

class EmptyRoutesListException : Exception("Source must contain at least one route")