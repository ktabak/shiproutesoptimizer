package pl.tabak.kn.shiproutesoptimizer.models

import org.geojson.Feature
import org.geojson.LineString
import org.geojson.LngLatAlt
import org.geojson.Point
import java.util.*

data class ShipRoute(
    val routeId: String,
    val vesselId: String,
    val fromPort: String,
    val toPort: String,
    val points: List<RoutePoint>
) {
    companion object {
        fun fromFeatureWithLines(feature: Feature) = ShipRoute(
            routeId = UUID.randomUUID().toString(),
            vesselId = feature.properties["vesselId"] as String,
            fromPort = feature.properties["from"] as String,
            toPort = feature.properties["to"] as String,
            points = (feature.geometry as LineString).coordinates.map {
                RoutePoint.fromLngLatAlt(it)
            }
        )

        fun toFeaturesListWithPoints(shipRoute: ShipRoute) =
            shipRoute.points.map {
                val representativeFeature = RoutePoint.toFeatureWithPoint(it)
                representativeFeature.properties = mapOf(
                    "vesselId" to shipRoute.vesselId,
                    "from" to shipRoute.fromPort,
                    "to" to shipRoute.toPort
                )
                representativeFeature
            }

        fun toFeatureWithRoute(shipRoute: ShipRoute): Feature {
            val feature = Feature()
            feature.geometry = LineString(*shipRoute.points.map { RoutePoint.toLngLatAlt(it) }.toTypedArray())
            feature.properties = mapOf(
                "vesselId" to shipRoute.vesselId,
                "from" to shipRoute.fromPort,
                "to" to shipRoute.toPort,
                "stroke" to "yellow",
                "stroke-opacity" to 1.0,
                "stroke-width" to 10
            )
            return feature
        }
    }
}

data class RoutePoint(
    val latitude: Double,
    val longitude: Double
) {
    companion object {
        fun fromLngLatAlt(lngLatAlt: LngLatAlt) = RoutePoint(
            latitude = lngLatAlt.latitude,
            longitude = lngLatAlt.longitude
        )

        fun toFeatureWithPoint(routePoint: RoutePoint): Feature {
            val feature = Feature()
            feature.geometry = Point(routePoint.longitude, routePoint.latitude)
            return feature
        }

        fun toLngLatAlt(routePoint: RoutePoint): LngLatAlt =
            LngLatAlt(routePoint.longitude, routePoint.latitude)
    }
}