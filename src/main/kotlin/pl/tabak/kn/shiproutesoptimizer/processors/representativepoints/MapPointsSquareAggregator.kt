package pl.tabak.kn.shiproutesoptimizer.processors.representativepoints

import java.math.BigDecimal

data class MapPointsSquareAggregator(
    val latitude: BigDecimal,
    val longitude: BigDecimal
)
