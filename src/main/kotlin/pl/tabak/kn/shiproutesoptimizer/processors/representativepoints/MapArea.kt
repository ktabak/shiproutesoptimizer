package pl.tabak.kn.shiproutesoptimizer.processors.representativepoints

import pl.tabak.kn.shiproutesoptimizer.models.RoutePoint

class MapArea(
    basicPoint: RoutePoint
) {
    var minLatitude: Double = basicPoint.latitude
    var maxLatitude: Double = basicPoint.latitude
    var minLongitude: Double = basicPoint.longitude
    var maxLongitude: Double = basicPoint.longitude

    fun updateArea(routePoint: RoutePoint) {
        if (routePoint.latitude < minLatitude) {
            minLatitude = routePoint.latitude
        } else if (routePoint.latitude > maxLatitude) {
            maxLatitude = routePoint.latitude
        }

        if (routePoint.longitude < minLongitude) {
            minLongitude = routePoint.longitude
        } else if (routePoint.longitude > maxLongitude) {
            maxLongitude = routePoint.longitude
        }
    }
}
