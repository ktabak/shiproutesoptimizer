package pl.tabak.kn.shiproutesoptimizer.processors

import org.springframework.stereotype.Component
import pl.tabak.kn.shiproutesoptimizer.configuration.properties.RepresentativePointsProcessorProperties
import pl.tabak.kn.shiproutesoptimizer.exceptions.EmptyRoutesListException
import pl.tabak.kn.shiproutesoptimizer.models.RoutePoint
import pl.tabak.kn.shiproutesoptimizer.models.ShipRoute
import pl.tabak.kn.shiproutesoptimizer.processors.representativepoints.MapPointsSquareAggregator
import pl.tabak.kn.shiproutesoptimizer.processors.representativepoints.RouteSquaresMapGenerator
import java.math.BigDecimal

@Component
class RepresentativePointsProcessor(
    private val routeSquaresMapGenerator: RouteSquaresMapGenerator,
    private val representativePointsProcessorProperties: RepresentativePointsProcessorProperties
) {
    fun calculateRepresentativePoints(shipRoutes: List<ShipRoute>): ShipRoute {
        if (shipRoutes.isEmpty()) {
            throw EmptyRoutesListException()
        }

        val routePoints = shipRoutes
            .filter { it.points.isNotEmpty() }
            .flatMap { it.points }

        val representativePoints = createListWithRepresentativePoints(routePoints)

        return shipRoutes.first().copy(points = representativePoints, vesselId = "representativePoints")
    }

    private fun createListWithRepresentativePoints(routePoints: List<RoutePoint>): List<RoutePoint> {
        val routeSquaresMap = routeSquaresMapGenerator.generateRouteSquaresMap(
            routePoints,
            representativePointsProcessorProperties.squareAggregatorSideLength
        )

        markSquaresWithRoutePoints(routePoints, routeSquaresMap)

        return calculateRepresentativePoints(routeSquaresMap)
    }

    private fun markSquaresWithRoutePoints(
        routePoints: List<RoutePoint>,
        routeSquaresMap: MutableMap<MapPointsSquareAggregator, Int>
    ) {
        routePoints.forEach {
            val roundedPointAggregator =
                roundPointToSquareAggregator(it, representativePointsProcessorProperties.squareAggregatorSideLength)

            routeSquaresMap[roundedPointAggregator]?.let { count ->
                routeSquaresMap[roundedPointAggregator] = count + 1
            }
        }
    }

    private fun calculateRepresentativePoints(
        routeSquaresMap: MutableMap<MapPointsSquareAggregator, Int>
    ): List<RoutePoint> {
        val squareSideHalfLength =
            representativePointsProcessorProperties.squareAggregatorSideLength.div(BigDecimal("2"))
        return routeSquaresMap
            .filter { (_, count) -> count > representativePointsProcessorProperties.squareAggregatorMinOccurrences }
            .map {
                RoutePoint(
                    latitude = it.key.latitude.add(squareSideHalfLength).toDouble(),
                    longitude = it.key.longitude.add(squareSideHalfLength).toDouble()
                )
            }
            .toList()
    }

    private fun roundPointToSquareAggregator(
        routePoint: RoutePoint,
        squareSideLength: BigDecimal
    ): MapPointsSquareAggregator {
        val roundedLatitude = getRoundedDimension(routePoint.latitude, squareSideLength)
        val roundedLongitude = getRoundedDimension(routePoint.longitude, squareSideLength)

        return MapPointsSquareAggregator(latitude = roundedLatitude, longitude = roundedLongitude)
    }

    private fun getRoundedDimension(dimension: Double, squareSideLength: BigDecimal): BigDecimal =
        BigDecimal.valueOf(dimension)
            .minus(
                BigDecimal.valueOf(dimension)
                    .rem(squareSideLength)
            )
}