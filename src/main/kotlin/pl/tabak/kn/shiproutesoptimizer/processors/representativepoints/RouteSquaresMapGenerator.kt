package pl.tabak.kn.shiproutesoptimizer.processors.representativepoints

import org.springframework.stereotype.Component
import pl.tabak.kn.shiproutesoptimizer.models.RoutePoint
import java.math.BigDecimal

@Component
class RouteSquaresMapGenerator {
    fun generateRouteSquaresMap(
        routePoints: List<RoutePoint>,
        squareSideLength: BigDecimal
    ): MutableMap<MapPointsSquareAggregator, Int> {
        val mapArea = MapArea(routePoints.first())
        routePoints.forEach { mapArea.updateArea(it) }

        return generateRouteSquaresMapFromMapArea(mapArea, squareSideLength)
    }

    private fun generateRouteSquaresMapFromMapArea(
        mapArea: MapArea,
        squareSideLength: BigDecimal
    ): MutableMap<MapPointsSquareAggregator, Int> {
        val routeSquaresMap: MutableMap<MapPointsSquareAggregator, Int> = HashMap()

        iterateThroughLatitudeToAddSquaresToMap(mapArea, squareSideLength, routeSquaresMap)
        return routeSquaresMap
    }

    private fun iterateThroughLatitudeToAddSquaresToMap(
        mapArea: MapArea,
        squareSideLength: BigDecimal,
        routeSquaresMap: MutableMap<MapPointsSquareAggregator, Int>
    ) {
        generateSequence(
            calculateMinimumMapPointDimension(
                mapArea.minLatitude,
                squareSideLength
            )
        ) {
            it.add(squareSideLength)
        }
            .takeWhile { it < BigDecimal.valueOf(mapArea.maxLatitude) }
            .forEach { latitude ->
                iterateThroughLongitudeAndAddSquaresToMap(
                    mapArea,
                    squareSideLength,
                    routeSquaresMap,
                    latitude
                )
            }
    }

    private fun iterateThroughLongitudeAndAddSquaresToMap(
        mapArea: MapArea,
        squareSideLength: BigDecimal,
        routeSquaresMap: MutableMap<MapPointsSquareAggregator, Int>,
        latitude: BigDecimal
    ) {
        generateSequence(
            calculateMinimumMapPointDimension(
                mapArea.minLongitude,
                squareSideLength
            )
        ) {
            it.add(squareSideLength)
        }
            .takeWhile { it < BigDecimal.valueOf(mapArea.maxLongitude) }
            .forEach { longitude ->
                addSquareToMap(routeSquaresMap, latitude, longitude)
            }
    }

    private fun addSquareToMap(
        routeSquaresMap: MutableMap<MapPointsSquareAggregator, Int>,
        latitude: BigDecimal,
        longitude: BigDecimal
    ) {
        routeSquaresMap[MapPointsSquareAggregator(
            latitude = latitude,
            longitude = longitude
        )] = 0
    }

    private fun calculateMinimumMapPointDimension(
        dimension: Double,
        squareSideLength: BigDecimal
    ) =
        BigDecimal.valueOf(dimension)
            .minus(
                BigDecimal.valueOf(dimension)
                    .rem(squareSideLength)
            )
}