package pl.tabak.kn.shiproutesoptimizer.processors.representativeroute

import org.geotools.referencing.GeodeticCalculator
import org.springframework.stereotype.Component
import pl.tabak.kn.shiproutesoptimizer.models.RoutePoint

@Component
class RouteLengthCalculator(
    private val geodeticCalculator: GeodeticCalculator = GeodeticCalculator()
) {
    fun calculateRouteLength(points: List<RoutePoint>): Double {
        var length = 0.0
        for (i in 0..points.size - 2) {
            length += calculateDistanceBetweenTwoPoints(points[i], points[i + 1])
        }
        return length
    }

    private fun calculateDistanceBetweenTwoPoints(first: RoutePoint, second: RoutePoint): Double{
        geodeticCalculator.setStartingGeographicPoint(first.longitude, first.latitude)
        geodeticCalculator.setDestinationGeographicPoint(second.longitude, second.latitude)
        return geodeticCalculator.orthodromicDistance
    }
}