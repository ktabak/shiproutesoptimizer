package pl.tabak.kn.shiproutesoptimizer.processors

import org.springframework.stereotype.Component
import pl.tabak.kn.shiproutesoptimizer.exceptions.EmptyRoutesListException
import pl.tabak.kn.shiproutesoptimizer.models.ShipRoute
import pl.tabak.kn.shiproutesoptimizer.processors.representativeroute.RouteLengthCalculator

@Component
class RepresentativeRouteProcessor(
    private val routeLengthCalculator: RouteLengthCalculator
) {
    fun calculateRepresentativeRoute(shipRoutes: List<ShipRoute>): ShipRoute {
        if (shipRoutes.isEmpty()) {
            throw EmptyRoutesListException()
        }

        return shipRoutes
            .map { RouteWithLengthAggregator(it, routeLengthCalculator.calculateRouteLength(it.points)) }
            .sortedBy { it.routeLength }[getIndexOfMiddleLengthShipRoute(shipRoutes.size)]
            .shipRoute
            .copy(vesselId = "representativeRoute")
    }

    private fun getIndexOfMiddleLengthShipRoute(numberOfRoutes: Int) =
        numberOfRoutes / 2

    data class RouteWithLengthAggregator(
        val shipRoute: ShipRoute,
        val routeLength: Double
    )
}