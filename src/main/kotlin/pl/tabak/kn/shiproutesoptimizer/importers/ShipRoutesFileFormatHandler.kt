package pl.tabak.kn.shiproutesoptimizer.importers

import pl.tabak.kn.shiproutesoptimizer.models.ShipRoute

interface ShipRoutesFileFormatHandler {
    fun importRoutesFromFile(filePath: String): List<ShipRoute>
    fun exportPointsToFile(filePath: String, shipRoute: ShipRoute)
    fun exportRouteToFile(filePath: String, shipRoute: ShipRoute)
}