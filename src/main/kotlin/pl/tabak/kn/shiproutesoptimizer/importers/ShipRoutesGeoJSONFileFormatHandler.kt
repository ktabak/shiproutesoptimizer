package pl.tabak.kn.shiproutesoptimizer.importers

import com.fasterxml.jackson.databind.ObjectMapper
import org.geojson.FeatureCollection
import pl.tabak.kn.shiproutesoptimizer.models.ShipRoute
import java.io.File

class ShipRoutesGeoJSONFileFormatHandler(
    private val objectMapper: ObjectMapper = ObjectMapper()
) : ShipRoutesFileFormatHandler {
    override fun importRoutesFromFile(filePath: String): List<ShipRoute> {
        val fileInputStream = File(filePath).inputStream()
        return objectMapper.readValue(fileInputStream, FeatureCollection::class.java)
            .features
            .map { ShipRoute.fromFeatureWithLines(it) }
    }

    override fun exportPointsToFile(filePath: String, shipRoute: ShipRoute) {
        val fileOutputStream = File(filePath).outputStream()
        objectMapper.writeValue(fileOutputStream, prepareFeatureCollectionWithPoints(shipRoute))
    }

    override fun exportRouteToFile(filePath: String, shipRoute: ShipRoute) {
        val fileOutputStream = File(filePath).outputStream()
        objectMapper.writeValue(fileOutputStream, prepareFeatureCollectionWithRoute(shipRoute))
    }

    private fun prepareFeatureCollectionWithPoints(shipRoute: ShipRoute): FeatureCollection {
        val featureCollection = FeatureCollection()
        featureCollection.addAll(ShipRoute.toFeaturesListWithPoints(shipRoute))
        return featureCollection
    }

    private fun prepareFeatureCollectionWithRoute(shipRoute: ShipRoute): FeatureCollection =
        FeatureCollection().add(ShipRoute.toFeatureWithRoute(shipRoute))
}