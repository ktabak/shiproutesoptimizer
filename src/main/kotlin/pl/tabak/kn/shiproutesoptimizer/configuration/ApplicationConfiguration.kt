package pl.tabak.kn.shiproutesoptimizer.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.tabak.kn.shiproutesoptimizer.ConsoleGeoJsonRunner
import pl.tabak.kn.shiproutesoptimizer.configuration.properties.ConsoleRunnerProperties
import pl.tabak.kn.shiproutesoptimizer.importers.ShipRoutesGeoJSONFileFormatHandler
import pl.tabak.kn.shiproutesoptimizer.processors.RepresentativePointsProcessor
import pl.tabak.kn.shiproutesoptimizer.processors.RepresentativeRouteProcessor

@Configuration
class ApplicationConfiguration(
    private val consoleRunnerProperties: ConsoleRunnerProperties,
    private val representativePointsProcessor: RepresentativePointsProcessor,
    private val representativeRouteProcessor: RepresentativeRouteProcessor
) {
    @Bean
    fun consoleGeoJsonRunner(): ConsoleGeoJsonRunner = ConsoleGeoJsonRunner(
        consoleRunnerProperties,
        representativePointsProcessor,
        representativeRouteProcessor,
        ShipRoutesGeoJSONFileFormatHandler()
    )
}