package pl.tabak.kn.shiproutesoptimizer.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "console-runner")
data class ConsoleRunnerProperties(
    var importRoutesFilePath: String = "",
    var exportPointsFilePath: String = "",
    var exportRouteFilePath: String = ""
)
