package pl.tabak.kn.shiproutesoptimizer.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import java.math.BigDecimal

@Configuration
@ConfigurationProperties(prefix = "representative-points-processor")
data class RepresentativePointsProcessorProperties(
    var squareAggregatorSideLength: BigDecimal = BigDecimal("0.03"),
    var squareAggregatorMinOccurrences: Int = 220,
)
