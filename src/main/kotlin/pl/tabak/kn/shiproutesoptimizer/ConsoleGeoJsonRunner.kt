package pl.tabak.kn.shiproutesoptimizer

import org.springframework.boot.CommandLineRunner
import pl.tabak.kn.shiproutesoptimizer.configuration.properties.ConsoleRunnerProperties
import pl.tabak.kn.shiproutesoptimizer.importers.ShipRoutesFileFormatHandler
import pl.tabak.kn.shiproutesoptimizer.models.ShipRoute
import pl.tabak.kn.shiproutesoptimizer.processors.RepresentativePointsProcessor
import pl.tabak.kn.shiproutesoptimizer.processors.RepresentativeRouteProcessor

class ConsoleGeoJsonRunner(
    private val consoleRunnerProperties: ConsoleRunnerProperties,
    private val representativePointsProcessor: RepresentativePointsProcessor,
    private val representativeRouteProcessor: RepresentativeRouteProcessor,
    private val shipRoutesFileFormatHandler: ShipRoutesFileFormatHandler
) : CommandLineRunner {
    override fun run(vararg args: String?) {
        val shipRoutes = shipRoutesFileFormatHandler.importRoutesFromFile(consoleRunnerProperties.importRoutesFilePath)

        generateRepresentativePoints(shipRoutes)
        generateRepresentativeRoute(shipRoutes)
    }

    private fun generateRepresentativeRoute(shipRoutes: List<ShipRoute>) {
        val representativeRoute = representativeRouteProcessor.calculateRepresentativeRoute(shipRoutes)
        shipRoutesFileFormatHandler.exportRouteToFile(
            consoleRunnerProperties.exportRouteFilePath,
            representativeRoute
        )
        println("Representative route calculated correctly. You can find generated GeoJSON here: " + consoleRunnerProperties.exportRouteFilePath)
    }

    private fun generateRepresentativePoints(shipRoutes: List<ShipRoute>) {
        val representativePoints = representativePointsProcessor.calculateRepresentativePoints(shipRoutes)
        shipRoutesFileFormatHandler.exportPointsToFile(
            consoleRunnerProperties.exportPointsFilePath,
            representativePoints
        )
        println("Representative points calculated correctly. You can find generated GeoJSON here: " + consoleRunnerProperties.exportPointsFilePath)
    }
}