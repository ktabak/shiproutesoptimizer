package pl.tabak.kn.shiproutesoptimizer.processors.representativepoints

import pl.tabak.kn.shiproutesoptimizer.models.RoutePoint
import spock.lang.Shared
import spock.lang.Specification

class MapAreaTest extends Specification {

    @Shared
    def basicPoint = new RoutePoint(50.00, 50.00)

    MapArea testMapArea

    def setup(){
        testMapArea = new MapArea(basicPoint)
    }

    def "should properly update map area minLatitude and minLongitude"() {
        given: "create point with latitude and longitude lower than basic point"
        def point = new RoutePoint(49.00, 48.00)

        when: "update map area"
        testMapArea.updateArea(point)

        then: "properly changed map area properties"
        with(testMapArea){
            minLatitude == point.latitude
            maxLatitude == basicPoint.latitude
            minLongitude == point.longitude
            maxLongitude == basicPoint.longitude
        }
    }

    def "should properly update map area maxLatitude and maxLongitude"() {
        given: "create point with latitude and longitude greater than basic point"
        def point = new RoutePoint(52.00, 55.00)

        when: "update map area"
        testMapArea.updateArea(point)

        then: "properly changed map area properties"
        with(testMapArea){
            minLatitude == basicPoint.latitude
            maxLatitude == point.latitude
            minLongitude == basicPoint.longitude
            maxLongitude == point.longitude
        }
    }
}
