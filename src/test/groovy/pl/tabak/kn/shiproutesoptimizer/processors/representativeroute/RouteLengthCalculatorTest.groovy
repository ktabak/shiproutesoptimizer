package pl.tabak.kn.shiproutesoptimizer.processors.representativeroute

import pl.tabak.kn.shiproutesoptimizer.models.RoutePoint
import spock.lang.Shared
import spock.lang.Specification

class RouteLengthCalculatorTest extends Specification {

    @Shared
    def routeLengthCalculator = new RouteLengthCalculator()

    def "should calculate route length with provided route points"() {
        given: "create list with sample route points"
        def routePoints = [
                new RoutePoint(50.00, 50.00),
                new RoutePoint(50.05, 50.07),
                new RoutePoint(50.06, 50.11),
                new RoutePoint(50.05, 50.07),
        ]

        when: "calculate route length"
        def result = routeLengthCalculator.calculateRouteLength(routePoints)

        then: "result contain properly calculated route length"
        result.round(2) == 13635.27 as Double
    }
}
