package pl.tabak.kn.shiproutesoptimizer.processors.representativepoints

import pl.tabak.kn.shiproutesoptimizer.models.RoutePoint
import spock.lang.Shared
import spock.lang.Specification

class RouteSquaresMapGeneratorTest extends Specification {

    @Shared
    def routeSquaresMapGenerator = new RouteSquaresMapGenerator()

    def "should properly generate route squares map"() {
        given: "create list with sample route points"
        def routePoints = [
                new RoutePoint(50.00, 50.50),
                new RoutePoint(50.50, 50.70),
                new RoutePoint(50.55, 50.90)
        ]

        and: "configure square side length"
        def squareSideLength = 0.3

        when: "generate route squares map for provided points list and square side length"
        def result = routeSquaresMapGenerator.generateRouteSquaresMap(routePoints, squareSideLength)

        then: "result contain properly generated squares map"
        result.size() == 6
        result.containsKey(new MapPointsSquareAggregator(49.8, 50.4))
        result.containsKey(new MapPointsSquareAggregator(49.8, 50.7))
        result.containsKey(new MapPointsSquareAggregator(50.1, 50.4))
        result.containsKey(new MapPointsSquareAggregator(50.1, 50.7))
        result.containsKey(new MapPointsSquareAggregator(50.4, 50.4))
        result.containsKey(new MapPointsSquareAggregator(50.4, 50.7))
    }
}
